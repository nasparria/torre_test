from django.contrib import admin
from django.urls import path
from django.http import HttpResponse
from myapp.views import get_user_data

def hello(request):
    return HttpResponse("Hello, World!")

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', hello, name='hello'),
    path('api/user/<str:username>/', get_user_data, name='get_user_data'),
]
