# yourapp/views.py
from django.http import JsonResponse
from .utilis import fetch_user_data

def get_user_data(request, username):
    user_data = fetch_user_data(username)
    if user_data:
        return JsonResponse(user_data)
    else:
        return JsonResponse({"error": "User not found"}, status=404)
