import requests

def get_user_skills(username):
    url = f'https://bio.torre.co/api/bios/{username}'
    response = requests.get(url)
    data = response.json()
    skills = []
    for key, value in data.items():
        skills.append({ 'name': key, 'value': value })
    return skills
