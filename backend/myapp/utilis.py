# yourapp/utils.py
import requests

def fetch_user_data(username):
    url = f"https://torre.bio/api/bios/{username}"
    response = requests.get(url)
    if response.status_code == 200:
        return response.json()
    return None
